$(function() {
		$(".currencyBox").hover(function() {
			$(this).addClass('active');
			$(".currency_detial").stop(true, true).delay(300).slideDown(500, "easeOutBounce");
			},  
			function() {
			$(".currency_detial").stop(true, true).delay(300).fadeOut(100, "easeInCubic");
		});
		$(".cart-block").hover(function() {
			$(this).addClass('active');
			$("#minicart").stop(true, true).delay(300).slideDown(500, "easeOutBounce");
			},  
			function() {
			$("#minicart").stop(true, true).delay(300).fadeOut(100, "easeInCubic");
		});
});
$(document).ready(function(){
		ddsmoothmenu.init({
		mainmenuid: "smoothmenu1", //menu DIV id
		orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
		classname: 'ddsmoothmenu', //class added to menu's outer DIV
		//customtheme: ["#1c5a80", "#18374a"],
		contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
	});
	
	  $('#mix').jcarousel({
			scroll: 1,
			easing: 'swing',
			animation: 750,
			visible: 0,
			auto: 0
		});	
		$('#like-pro').jcarousel({
			scroll: 1,
			easing: 'swing',
			animation: 750,
			visible: 0,
			auto: 0
		});
		$('#related-pro').jcarousel({
			scroll: 1,
			easing: 'swing',
			animation: 750,
			visible: 0,
			auto: 0
		});
		$('#moreView').jcarousel({
			scroll: 1,
			easing: 'swing',
			animation: 750,
			visible: 0,
			auto: 0
		});


	$('.menuBox').click(function() {
			console.log("menuBox");
			if ($('#menuInnner').is(":hidden"))
			{
				$('#menuInnner').slideDown("fast");
			} else {
				$('#menuInnner').slideUp("fast");
			}
			return false;
			});
			if($(window).width() <= 1000){
				$('#smoothmenu1').hide();
				$('.mobMenu').show();
			}else{
				$('#smoothmenu1').show();
				$('.mobMenu').hide();
				}

	/* --------------------------
		ADD BY THINH 
		------------------------*/
		
	// Product detail popup

	$('.product-grid .pro-img').on({
		mouseenter: function() {
			// var prdId = $(this).parent().parent().attr('data-prdid');
			$(this).parent().find(".popup-detail").fadeIn(200);
		},
		mousemove: function(e){
			var toRightBorder = $(window).width() - e.clientX;
			if(toRightBorder < 310) {
				// console.log(e.clientX+", "+e.clientY);
				$(this).parent().find(".popup-detail").css({
					top: e.clientY + 10,
					left: e.clientX - 310
				})
			}else {
				// console.log(e.clientX+", "+e.clientY);
				$(this).parent().find(".popup-detail").css({
					top: e.clientY + 10,
					left: e.clientX + 10
				})
			}
		},
		mouseleave: function() {
			$('.popup-detail').fadeOut(10);
		}
	});

	$('.advance-filter .classify').on('click', function() {
		var target = $(this).attr('data-collapse-target');
		$(this).parent().find(target).toggle(200);
		$(this).toggleClass('open');
	});
	
	$( "#slider-range" ).slider({
		range: true,
		min: 100000,
		max: 5000000,
		step: 100000,
		values: [ 1000000, 3000000 ],
		slide: function( event, ui ) {
			$( "#amount" ).val( moneyFormat(ui.values[ 0 ]) + " đ - " + moneyFormat(ui.values[ 1 ]) + " đ");
		}
	});

	function moneyFormat(value) {
	    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
	}

	$( "#amount" ).val( moneyFormat($( "#slider-range" ).slider( "values", 0 )) + " đ - " + moneyFormat($( "#slider-range" ).slider( "values", 1 )) + " đ");

	/*  Collapse List */
	$(window).on("load", function(){
		var width = $(this).width();
		if ( width < 768) {
			$('.custom-collapse-list .level-1 .collapse-toggle').bind('click', function(){
				var state = $(this).parent().find('.level-2').css('display');
				if (state === "block") {
					$(this).parent().find('.level-2').slideUp('fast');
				} else {
					$(this).parent().parent().find('.level-2').slideUp('fast');
					$(this).parent().find('.level-2').slideDown('fast');
				}
			});
		} else {
			$('.custom-collapse-list .level-1').unbind('click');
		}
	});


	/* OWL-SLIDER */
	$("#index-slider").owlCarousel({
		// Most important owl features
		items : 1,
		itemsCustom : false,
		itemsDesktop : [1199,1],
		itemsDesktopSmall : [991,1],
		itemsTablet: [767,1],
		itemsTabletSmall: false,
		itemsMobile : [479,1],
		singleItem : true,
		itemsScaleUp : false,

		//Basic Speeds
		slideSpeed : 1000,
		paginationSpeed : 1000,
		rewindSpeed : 500,

		//Autoplay
		autoPlay : 5000,
		stopOnHover : true,

		// Navigation
		navigation : false,
		navigationText : ["prev","next"],
		rewindNav : true,
		scrollPerPage : false,

		//Pagination
		pagination : false,
		paginationNumbers: false,

		// Responsive 
		responsive: true,
		responsiveRefreshRate : 100,
		responsiveBaseWidth: window,

		// CSS Styles
		baseClass : "owl-carousel",
		theme : "owl-theme",

		//Lazy load
		lazyLoad : false,
		lazyFollow : true,
		lazyEffect : "fade",

		//Auto height
		autoHeight : false,

		//JSON 
		jsonPath : false, 
		jsonSuccess : false,

		//Mouse Events
		dragBeforeAnimFinish : true,
		mouseDrag : true,
		touchDrag : true,

		//Transitions
		transitionStyle : "fadeUp",

		// Other
		addClassActive : false,
	});
	$("#categories-slider").owlCarousel({
		// Most important owl features
		items : 1,
		itemsCustom : false,
		itemsDesktop : [1199,1],
		itemsDesktopSmall : [991,1],
		itemsTablet: [767,1],
		itemsTabletSmall: false,
		itemsMobile : [479,1],
		singleItem : true,
		itemsScaleUp : false,

		//Basic Speeds
		slideSpeed : 1000,
		paginationSpeed : 1000,
		rewindSpeed : 500,

		//Autoplay
		autoPlay : 5000,
		stopOnHover : true,

		// Navigation
		navigation : false,
		navigationText : ["prev","next"],
		rewindNav : true,
		scrollPerPage : false,

		//Pagination
		pagination : false,
		paginationNumbers: false,

		// Responsive 
		responsive: true,
		responsiveRefreshRate : 100,
		responsiveBaseWidth: window,

		// CSS Styles
		baseClass : "owl-carousel",
		theme : "owl-theme",

		//Lazy load
		lazyLoad : false,
		lazyFollow : true,
		lazyEffect : "fade",

		//Auto height
		autoHeight : false,

		//JSON 
		jsonPath : false, 
		jsonSuccess : false,

		//Mouse Events
		dragBeforeAnimFinish : true,
		mouseDrag : true,
		touchDrag : true,

		//Transitions
		transitionStyle : "fadeUp",

		// Other
		addClassActive : false,
	});
	$("#brands-slider .inner").owlCarousel({
		// Most important owl features
		items : 6,
		itemsCustom : false,
		itemsDesktop : [1199,6],
		itemsDesktopSmall : [991,5],
		itemsTablet: [767,5],
		itemsTabletSmall: false,
		itemsMobile : [479,4],
		singleItem : false,
		itemsScaleUp : false,

		//Basic Speeds
		slideSpeed : 1000,
		paginationSpeed : 1000,
		rewindSpeed : 500,

		//Autoplay
		autoPlay : 5000,
		stopOnHover : true,

		// Navigation
		navigation : false,
		navigationText : ["prev","next"],
		rewindNav : true,
		scrollPerPage : false,

		//Pagination
		pagination : false,
		paginationNumbers: false,

		// Responsive 
		responsive: true,
		responsiveRefreshRate : 100,
		responsiveBaseWidth: window,

		// CSS Styles
		baseClass : "owl-carousel",
		theme : "owl-theme",

		//Lazy load
		lazyLoad : false,
		lazyFollow : true,
		lazyEffect : "fade",

		//Auto height
		autoHeight : false,

		//JSON 
		jsonPath : false, 
		jsonSuccess : false,

		//Mouse Events
		dragBeforeAnimFinish : true,
		mouseDrag : true,
		touchDrag : true,

		//Transitions
		transitionStyle : "fadeUp",

		// Other
		addClassActive : false,
	});
});

$(window).on('load', function(){
	/* Vertical-Centralize brands */
	$("#brands-slider").on('custom-st-load', function(){
		$("#brands-slider .media-container").each(function(){
			var imgHeight = $(this).children('img').height();
			// console.log(imgHeight);
			if(imgHeight < 174) {
				var marginTop = (174-imgHeight)/2;
				$(this).children('img').css('margin-top',marginTop)
			}
		});
	});
	$('#brands-slider').trigger("custom-st-load");
})

jQuery(function() {
	var jQueryallVideos = jQuery("iframe[src^='http://player.vimeo.com']"),jQueryfluidEl = jQuery(".about-banner");
	jQueryallVideos.each(function() {
		jQuery(this)
			.data('aspectRatio', this.height / this.width)
			.removeAttr('height')
			.removeAttr('width');
	});
	jQuery(window).resize(function() {
		var newWidth = jQueryfluidEl.width();
		jQueryallVideos.each(function() {
			var jQueryel = jQuery(this);
			jQueryel
				.width(newWidth)
				.height(newWidth * jQueryel.data('aspectRatio'));
		});
		if($(window).width() <= 1000){
			$('#smoothmenu1').hide();
			$('.mobMenu').show();
		}else{
			$('#smoothmenu1').show();
			$('.mobMenu').hide();
			}
	}).resize();

});
